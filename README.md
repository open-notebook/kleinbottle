open notebook 2: Lagrangian Klein bottles
=========================================

This is an open research notebook.

I plan to post:
- problems I'm thinking about,
- thoughts I've had/progress I've made on these problems.
If you have any thoughts or comments, I'd be very happy to hear from
you (either via email, via bug reports or via a git pull request).

The remit of this particular notebook is the topic of Lagrangian Klein
bottles in symplectic 4-manifolds.

Overview
--------

The symplectic manifold $`X=S^2\times S^2`$ admits a 1-parameter
family of symplectic forms
$`\omega_\lambda=\lambda\omega_{S^2}\oplus\omega_{S^2}`$ (where
$`\omega_{S^2}`$ is the standard area form on $`S^2`$ having area
$`4\pi`$ and $`\lambda`$ is the parameter). Gromov [Gromov] showed
that the symplectomorphism group of $`(X,\omega_\lambda)`$ has finite
fundamental group when $`\lambda=1`$ and infinite fundamental group
when $`\lambda>1`$. Abreu-McDuff [AbreuMcDuff] showed that the
rational cohomology of this symplectomorphism group changes whenever
$`\lambda`$ crosses an integer value (but stays constant otherwise).

If we look instead at the "Lagrangian content" (i.e. the set of
Lagrangian submanifolds, possibly singular, which appear in
$`(X,\omega_\lambda)`$) then we see some interesting phenomena at
other rational values of $`\lambda`$. 

One easy way to produce Lagrangian subsets is when you have a
toric/almost toric fibration and you look for "visible Lagrangians"
living over arcs in the base of the diagram. See Symington's original
paper [Symington] or my notes on Lagrangian torus fibrations
[LTFs]. Over each point of the interior of the arc, you get a
circle. At the endpoint of the arc, this circle collapses onto either
a circle (if the endpoint sits on the boundary of the moment polygon)
or a point (if the endpoint sits at a vertex). In the first case, you
get either a Mobius strip (if the circle collapses 2-to-1 onto the
boundary circle) or a "pinwheel core" where several flanges meet
along a circle. In the second case, you get a Lagrangian topological
disc which is the cone on a Legendrian knot. This can be smooth, or it
can give a singularity called a Schoen-Wolfson singularity. These
cones have been studied by Schoen and Wolfson [Schoen-Wolfson1]: they
are Hamiltonian minimising for area.

If you allow Lagrangians which are locally modelled on pinwheel cores
or Schoen-Wolfson singularities, you start to see a lot more
interesting stuff happening at other rational values of
$`\lambda`$. For example, if $`\lambda=p/q>1`$ (with $`p,q`$ positive
coprime integers) then there is a Lagrangian "sphere" in the
homology class $`(q,-p)`$ with two Schoen-Wolfson singularities (of
type $`(p,q)`$ ). Note that such a singular sphere defines a homology
class because the singularities are isolated (they're modelled on
cones on certain Legendrian $`(p,q)`$ -torus knots). This is only
possible for this value of $`\lambda`$ because that's the only case
when this homology class is orthogonal to the symplectic form. You can
see the singular sphere as a visible Lagrangian living over the
diagonal with slope $`p/q`$ in the rectangular moment image of
$`S^2\times S^2`$ (which is a rectangle with side lengths $`1`$ and
$`p/q`$).

When $`\lambda=1`$, there is a Lagrangian Klein bottle which is
visible in the standard moment square: it lives over a line of slope
$`2`$ joining the bottom edge of the square to the top. When
$`\lambda=2`$ , it becomes impossible to draw such a line: it
necessarily goes through the vertices of the moment rectangle. You can
imagine the Mobius cores of the Klein bottle collapsing down to
singular points (of Schoen-Wolfson type). For $`\lambda>2`$ , even that
fails. Of course, something similar should happen for every other
rational number $`\lambda`$, but the corresponding Lagrangian will be
made up of two pinwheel cores instead of two Moebius strips, so I want
to consider the case of Klein bottles first.

**Conjecture:** There is **no Lagrangian Klein bottle** in this
$`\mathbf{Z}/2`$-homology class when $`\lambda\geq 2`$ (and similar
phenomena should happen for Lagrangian "bi-pinwheels" for other
$`p/q`$ , where we work with homology mod $`p`$ to get a well-defined
fundamental class).

I can currently only prove something strictly weaker (see klein.pdf).

Note that there is another homology class in $`S^2\times S^2`$ which
**does** contain a (visible) Lagrangian Klein bottle when
$`\lambda\geq 2`$ (living over a line of slope $`1/2`$ ). That's not a
counterexample to my conjecture (which specifies the homology class).

Other related work
------------------

The classic problem of whether or not the Klein bottle embeds as a
Lagrangian in $`\mathbf{R}^4`$ was resolved in the negative by
Shevchishin [Shevchishin] with an alternative proof given later by
Nemirovsky [Nemirovsky]. There was an earlier attempt by Mohnke
[Mohnke] to prove this using SFT (and my argument in klein.pdf uses
some of Mohnke's ideas and lemmas).

Some recent papers which study when a given $`\mathbf{Z}/2`$-homology
class can be represented by a nonorientable Lagrangian (and how this
depends on the cohomology class of $`\omega`$) are:
- arXiv:1902.08901 [DaiHoLi] (who construct nonorientable Lagrangians
  of potentially high genus in any homology class which has no
  obstructions to containing them).
- arXiv:1908.10895 [ShevchishinSmirnov] (who give necessary and
  sufficient conditions on the cohomology class of $`\omega`$ for
  finding a Lagrangian $`\mathbf[RP}^2`$ in the ball blown up three
  times).

There is also a nice connection to the theory of minimal
surfaces. Schoen and Wolfson considered the problem of minimising area
over the class of orientable Lagrangian currents in a Kaehler
4-manifold. They prove an existence result: any homology class
contains a singular Lagrangian which minimises area, where you allow
immersed points and Schoen-Wolfson singularities. When you allow
nonorientable guys, the problem becomes harder [SchoenWolfson2]: see
this interesting paper [Qiu] on constructing Moebius strips with low
area.

References
----------

- [AbreuMcDuff] Abreu, M. and McDuff, D. Topology of symplectomorphism
  groups of rational ruled surfaces. arXiv:math/9910057v1
- [DaiHoLi] Dai, B., Ho, C-I. and Li, T-J. Non-orientable Lagrangian
  surfaces in rational 4-manifolds arXiv:1902.08901
- [Gromov] Gromov, M. Pseudoholomorphic curves in symplectic
  manifolds, Invent. Math 82 (1985) 307--347.
- [LTFs] My lecture notes on Lagrangian torus fibrations
  http://jde27.uk/misc/ltf.pdf
- [Qiu] Qiu, W. Non-orientable Lagrangian surfaces with controlled
  area, MRL 8 693--701 (2001) [Full text
  here](https://www.intlpress.com/site/pub/files/_fulltext/journals/mrl/2001/0008/0006/MRL-2001-0008-0006-a001.pdf)
- [ShevchishinSmirnov] Shevchishin, V., Smirnov, G. Symplectic
  triangle inequality https://arxiv.org/abs/1908.10895
- [SchoenWolfson1] Schoen, R. and Wolfson, J. Minimizing area among
  Lagrangian surfaces: the mapping problem
  https://arxiv.org/abs/math/0008244
- [SchoenWolfson2] Schoen, R. and Wolfson, J. The volume functional
  for Lagrangian submanifolds. Lectures on partial differential
  equations, 181--191, New Stud. Adv. Math., 2, Int. Press,
  Somerville, MA, 2003.
- [Symington] Symington, M. Four dimensions from two in symplectic
  topology. https://arxiv.org/abs/math/0210033
