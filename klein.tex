\documentclass{article}
\title{A nonsqueezable Lagrangian Klein bottle}
\include{head}
\begin{document}
\maketitle
\section{Introduction}


For each connected interval \(I\subset\RR\), let \(C_I\) denote the
cylinder \(I\times(\RR/2\pi\ZZ)\), with coordinates \((p,q)\),
equipped with the symplectic form \(dp\wedge dq\). Let \(S^2\) denote
the unit sphere \(\{(x,y,z)\in\RR^3\ :\ x^2+y^2+z^2=1\}\), equipped
with its area form \(\sigma\) satisfying \(\int_{S^2}\sigma=4\pi\);
let \((z,\phi)\) be cylindrical coordinates on the sphere (so
\(x=\sqrt{1-z^2}\cos\phi\), \(y=\sqrt{1-z^2}\sin\phi\)).


Consider the symplectic manifold \((U_I,\omega):=(C_I\times
S^2,dp\wedge dq+\sigma)\). This manifold admits a Hamiltonian torus
action
\((e^{it_1},e^{it_2})\cdot(p,q,z,\phi)=(p,q+t_1,z,\phi+t_2)\). The
moment map for this action is \(\mu(p,q,z,\phi)=(p,z)\), and the image
of the moment map is the strip \(I\times[-1,1]\).


\begin{Lemma}\label{lma:klein}
There is a Lagrangian Klein bottle \(L\) in \(U_{[0,1]}\) whose
projection along \(\mu\) is the line \(\ell\) (dotted below) of
slope 2 from \((0,-1)\) to \((1,1)\).


\tka
\fill[gray!50] (0,-1) -- (1,-1) -- (1,1) -- (0,1) -- cycle;
\draw[thick] (0,-1) -- (1,-1);
\draw[thick] (0,1) -- (1,1);
\draw[thick,dotted] (0,-1) -- (1,1);
\node at (1/2,0) [left] {\(\ell\)};
\tkz
\end{Lemma}
\begin{Remark}\label{rmk:lma:klein}
This \(L\) is a {\em visible Lagrangian} in the sense of Symington
\cite{Symington}.
\end{Remark}
\begin{Proof}[Proof of \cref{lma:klein}]\label{prf:lma:klein}
The line \(\ell\) has equation \(z=2p\). The Lagrangian Klein bottle
\(L\) is cut out by the equations \(z=2p\) and
\(\phi=-\frac{1}{2}q\). Since \((p,q,z,\phi)\) are action-angle
coordinates, the symplectic form is just \(dp\wedge dq+dz\wedge
d\phi\), which makes it easy to check that \(L\) is Lagrangian. To
see that \(L\) is a Klein bottle, notice that the regular level sets
of \(z\) restricted to \(L\) are circles \(\phi=-q/2\) in the
\((q,\phi)\)-torus, which collapse \(2\)-to-\(1\) onto the circles
of maxima and minima at \(z=\pm 1\) (as the torus collapses to the
circle with coordinate \(q\)). \qedhere


\end{Proof}
By contrast, we have:


\begin{Theorem}\label{thm:klein}
There is no Lagrangian embedding \(\iota\colon K\to U_{(0,1)}\) of
the Klein bottle \(K\) in \(U_{(0,1)}\) such that \(\iota_*\colon
H_1(K;\QQ)\to H_1(U_{(0,1)};\QQ)\) is injective.


\end{Theorem}
\begin{Remark}
Note that \(H_1(L;\QQ)\to H_1(U_{[0,1]};\QQ)\) is injective for the
Lagrangian Klein bottle \(L\) in \cref{lma:klein}: the loop
\(\mu^{-1}(1,1)\) is a generator for both.


\end{Remark}
\section{Proof of theorem}


\subsection{Mohnke's almost complex structure}


Pick a flat metric \(g\) on the Klein bottle. There is a contact form
(the canonical 1-form) on the unit cotangent bundle \(M\subset T^*K\)
whose closed Reeb orbits correspond to closed geodesics on \(K\). We
will not distinguish notationally between geodesics and the
corresponding Reeb orbits and we will write \(-\gamma\) for the
geodesic obtained by reversing \(\gamma\). There are two isolated
simple geodesics \(\gamma_0,\gamma_1\) which are the core circles for
two disjoint embedded M\"{o}bius strips in \(K\). Any isolated
geodesic is a multiple cover of one of these and all other geodesics
occur in one-parameter families. We call the isolated geodesics {\em
odd} and the other geodesics {\em even}.


\begin{Theorem}[Mohnke {\cite[Section 2.1]{Mohnke}}]\label{thm:mohnke}
There exists an almost complex structure \(J^-\) on the cotangent
bundle \(T^*K\) with the following properties:


\begin{enumerate}
\item \(J^-\) is cylindrical at infinity and suitable for
neck-stretching.


\item For any geodesic \(\gamma\) there is a finite-energy
\(J^-\)-holomorphic cylinder \(f_\gamma\) in \(T^*K\) asymptotic
to \(\gamma\) and \(-\gamma\).


\item {\cite[Lemma 7(2)]{Mohnke}} Any \(J^-\)-holomorphic cylinder in
\(T^*K\) which intersects the zero-section is one of these
\(f_\gamma\) for some closed geodesic \(\gamma\).


\end{enumerate}
\end{Theorem}
\begin{Remark}\label{rmk:intersection}
If we let \(W:=\overline{T^*K}\) denote the projective completion of
the cotangent bundle and \(M\subset W\) denote the ideal contact
boundary then there is a well-defined intersection pairing
\(H_2(W,M;\ZZ/2)\otimes H_2(W;\ZZ/2)\to\ZZ/2\). The cylinders
\(f_\gamma\) define elements of \(H_2(W,M;\ZZ/2)\) and we have
{\cite[Lemma 7(3)]{Mohnke}} \[f_\gamma\cdot K=\begin{cases} 1\mbox{
if }\gamma\mbox{ is odd}\\ 0\mbox{ if }\gamma\mbox{ is
even.}\end{cases}\]


\end{Remark}
\begin{Remark}[{\cite[Lemma 7(1)]{Mohnke}}]\label{rmk:noplanes}
Note that there are also no finite energy planes in \(T^*K\), nor in
the symplectisation \(\RR\times M\), for {\em any} cylindrical
almost complex structure adapted to our chosen contact form. This is
because there are no contractible Reeb orbits, and a finite energy
plane would provide a nullhomotopy of its asymptote.


\end{Remark}
\subsection{Neck-stretching}


Suppose there is a Lagrangian Klein bottle \(K\subset U_{(0,1)}\) such
that \(H_1(K;\QQ)\to H_1(U_{(0,1)};\QQ)\) is injective. Think of \(K\)
sitting inside \(U_{[0,1]}\) and make symplectic cuts to \(U_{[0,1]}\)
at \(p=0,1\) to obtain a Lagrangian Klein bottle \(K\) living in the
manifold \(X=S^2\times S^2\) equipped with the product symplectic form
giving the factors areas \(2\pi\) and \(4\pi\) respectively. We
rescale the symplectic form to make these areas \(1\) and \(2\) for
simplicity. Crucially, the symplectic cut introduces symplectic
spheres \(S_0\) and \(S_1\) (at the \(p=0,1\) cuts respectively) which
are disjoint from \(K\).


Pick a sequence of almost complex structures \(J_t\), \(t\in\RR\), on
\(X\) with the following properties:
\begin{itemize}
\item on a Weinstein neighbourhood of \(K\), \(J_t\) coincides with
Mohnke's almost complex structure \(J^-\);
\item on a neck-stretching region \((a_t,b_t)\times M\) around \(K\),
\(J_t\) is a neck-stretching sequence;
\item the spheres \(S_0,S_1\) are \(J_t\)-holomorphic for all
\(t\in\RR\).
\end{itemize}
Pick a point \(k\) on \(K\) which does not lie on any of the cylinders
\(f_{\gamma}\) for an odd geodesic \(\gamma\). Let \(u_t\colon S^2\to
X\) be a \(J_t\)-holomorphic curve representing the class
\(\alpha=[S^2\times\{\star\}]\) and such that \(u_t(0)=k\) (there is a
unique such \(u_t\) up to reparametrisation by a theorem of Gromov
{\cite[2.4.C]{Gromov}}, since \(\alpha\) is a minimal area sphere
class in \(X\)).


By the SFT compactness theorem \cite{BEHWZ}, there is a sequence
\(t_i\) such that \(u_{t_i}\) converges (after reparametrisations) to
a holomorphic building with components in \(T^*K\) (the completion of
the Weinstein neighbourhood of \(K\)), components in \(\RR\times M\)
(the completion of the neck) and components in \(X\setminus K\) (the
completion of the complement of the Weinstein neighbourhood).


\subsection{SFT limit analysis}


The components \(v_1,\ldots,v_n\) of the SFT limit building living in
\(X\setminus K\) can be compactified, yielding topological surfaces in
\(X\) with boundary on \(K\); we will still denote these by
\(v_1,\ldots,v_n\). The sum of the \(\omega\)-areas of the \(v_i\)
(weighted by branch multiplicities if the SFT limit involves a
branched cover) equals the \(\omega\)-area of \(\alpha\), which is
\(1\).


\begin{Lemma}\label{lma:atleasttwoplanes}
There must be at least two planar components amongst the \(v_i\).
\end{Lemma}
\begin{Proof}
First note that the limit building intersects \(K\) because
\(u_t(0)=k\in K\) for all \(t\). It also necessarily has at least
one component in \(X\setminus K\) because \(T^*K\) is exact and so
contains no closed holomorphic curves. A genus zero holomorphic
building with at least two levels must have two planar components
(just for topological reasons) though these could be geometrically
indistinct. Any planar components live in \(X\setminus K\). \qedhere


\end{Proof}
\begin{Lemma}\label{lma:exactlytwoplanes}
There are two components \(v_0,v_1\) of the limit building such that
\(v_i\cdot S_j=\delta_{ij}\). These components are planar and there
are no further components of the limit building in \(X\setminus L\).
\end{Lemma}
\begin{Proof}
Since \(\alpha\) intersects \(S_0\) and \(S_1\) there must be
components of the limit building which intersect \(S_0\) and
\(S_1\). By positivity of intersections, either:


\begin{enumerate}
\item [(A)] there is one component \(v_1\) which hits both \(S_0\) and
\(S_1\) once transversely and all other components are disjoint
from \(S_0,S_1\).


\item [(B)] there are two components \(v_0,v_1\) such that \(v_0\)
intersects \(S_0\) once transversely and is disjoint from \(S_1\)
and vice versa for \(v_1\).


\end{enumerate}
Moreover, each of these components occurs with multiplicity one in
the SFT limit in order to get the correct intersection numbers
\(\alpha\cdot S_0,\alpha\cdot S_1\).


If \(v_2\) is a component does not intersect \(S_0\) or \(S_1\) then
it defines a class in \(H_2(U_{(0,1)},K;\ZZ)\). By assumption, the
kernel of the map \(\ZZ\oplus\ZZ/2=H_1(K;\ZZ)\to
H_1(U_{(0,1)};\ZZ)=\ZZ\) is precisely the torsion part. Therefore
the long exact sequence \[\cdots\to H_2(U_{(0,1)};\ZZ)\to
H_2(U_{(0,1)},K;\ZZ)\to H_1(K;\ZZ)\to\cdots\] splits off a sequence
\[\cdots\to\ZZ\to H_2(U_{(0,1)},K;\ZZ)\to\ZZ/2\to 0.\] This implies
that the areas of classes in \(H_2(U_{(0,1)},K;\ZZ)\) are
half-integer multiples of the area of the generator \(\beta\in
H_2(U_{(0,1)};\ZZ)\), which is \(2\). Therefore \(v_2\) has integer
area. Since the area of \(\alpha\) is \(1\), and since \(v_1\)
already has positive area, there cannot be a component \(v_2\).


By \cref{lma:atleasttwoplanes}, there are at least two planar
components (or one planar component with multiplicity two) in the
limit building. This is not compatible with Case (A), so we must be
in Case (B) and \(v_0,v_1\) must additionally be planes. \qedhere


\end{Proof}
\begin{Lemma}
\begin{enumerate}
\item All the remaining parts of the limit building are cylinders.


\item At least one of these cylinders lives in \(T^*K\) and has the form
\(f_{\gamma}\) for an odd geodesic \(\gamma\).


\item There are no other cylindrical components of the SFT limit
building in \(T^*K\).
\end{enumerate}
\end{Lemma}
\begin{Proof}
\begin{enumerate}
\item If a component has three or more punctures then the limit building
must contain at least three planar components (counted with
multiplicity) but we have seen that all the planar components must
live in \(X\setminus K\) (\cref{rmk:noplanes}) and that there are
precisely two such components (\cref{lma:exactlytwoplanes}).


\item Since \(u_t(0)=k\) for all \(t\), the limit building contains a
component in \(T^*K\), which must be a cylinder of the form
\(f_\gamma\) by \cref{thm:mohnke}(3). At least one of these
cylindrical components must correspond to an odd geodesic because
\(\alpha\) has odd intersection with \(K\) in \(H_2(X;\ZZ/2)\) and
the intersection number picks up contributions from each component
of the building inside \(T^*K\), which are nontrivial if and only
if \(\gamma\) is odd (\cref{rmk:intersection}).


\item If there are two or more cylindrical components in \(T^*K\) then
there must be a further cylindrical component in \(\RR\times M\)
which connects the asymptotes of two of these cylinders. Since
this cylinder has no positive asymptote, this cannot exist by the
maximum principle. \qedhere


\end{enumerate}
\end{Proof}
\begin{Proof}[Proof of \cref{thm:klein}]\label{prf:thm:klein}
We chose \(k\in K\) not to lie on any of the cylinders \(f_\gamma\)
for \(\gamma\) an odd geodesic, but we have showed that these are
the only cylinders which can arise as components of the SFT limit
building. Since the SFT limit building must pass through \(k\), we
get a contradiction. \qedhere


\end{Proof}
\bibliographystyle{plain}
\bibliography{klein}
\end{document}
